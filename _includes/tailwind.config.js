// See the Tailwind default theme values here:
// https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
const colors = require("tailwindcss/colors")
const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  theme: {
    extend: {
      colors: {
        "primary-200": "#eef1ff",
        "primary-500": "#4965f6",
        "primary-600": "#1231d0",
        "secondary-200": "#e3f6ed",
        "secondary-500": "#3ecf8e",
        "secondary-600": "#36b47c",
        "tertiary-200": "#eeeeee",
        "tertiary-500": "#57586e",
        "tertiary-600": "#3a3b48",
        "danger-200": "#fff3f3",
        "danger-500": "#F37B7B",
        "danger-600": "#d26969",
        "code-400": "#fefcf9",
        "code-600": "#3c455b"
      },
    },
    fontFamily: {
      sans: ["Inter var", ...defaultTheme.fontFamily.sans],
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/aspect-ratio"),
  ],
  purge: ["./_site/**/*.html"],
  darkMode: false, // or 'media' or 'class'
}
